/* eslint-disable react-native/no-inline-styles */
import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {Dimensions, SafeAreaView, StyleSheet, View} from 'react-native';
import {Header} from '../../common/Header';
import {OVButton} from '../../components/OVButton';
import OVText, {medium, poppinsRegular} from '../../components/OVText';
import {OVTextInput} from '../../components/OVTextInput';
import {APP_THEME_COLOR, BG_COLOR, GRAY_700, WHITE} from '../../utils/Colors';

const windowHeight = Dimensions.get('window').height;
const windowWidth = Dimensions.get('window').width;

const ChangePassword = () => {
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: BG_COLOR}}>
      <Header
        isHome={false}
        navigation={navigation}
        onBackPressed={() => navigation.goBack()}
        title="Change Password"
      />
      <View style={{padding: 20, flexDirection: 'column'}}>
        <OVText
          size={medium}
          fontType={poppinsRegular}
          color={GRAY_700}
          style={{marginTop: 20}}>
          Old Password
        </OVText>
        <OVTextInput editable={true} keyboardType="email-address" />
        <OVText
          size={medium}
          fontType={poppinsRegular}
          color={GRAY_700}
          style={{marginTop: 20}}>
          New Password
        </OVText>
        <OVTextInput editable={true} keyboardType="email-address" />
        <OVText
          size={medium}
          fontType={poppinsRegular}
          color={GRAY_700}
          style={{marginTop: 20}}>
          Confirm Password
        </OVText>
        <OVTextInput editable={true} keyboardType="email-address" />

        <OVButton
          title="Submit"
          color={APP_THEME_COLOR}
          textColor={WHITE}
          marginTop={40}
          marginBottom={20}
          onPress={() => navigation.goBack()}
          width={windowWidth - 30}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  textInputContainer: {
    width: '80%',
    marginTop: 20,
    backgroundColor: 'rgba(52, 52, 52, 0.3)',
    borderRadius: 10,
  },
  textField: {
    color: WHITE,
    fontFamily: 'Poppins-Regular',
    textAlign: 'center',
    width: '100%',
  },
});

export default ChangePassword;
