/* eslint-disable react-native/no-inline-styles */
import React, {useContext, useEffect, useState} from 'react';
import {
  Dimensions,
  FlatList,
  Image,
  SafeAreaView,
  ScrollView,
  View,
} from 'react-native';
import OVText, {
  medium,
  poppinsMedium,
  poppinsRegular,
  small,
} from '../../components/OVText';
import {DOCTOR_1, DOCTOR_2, APP_ICON} from '../../images';
import {
  APP_THEME_COLOR,
  BG_COLOR,
  BLACK,
  LIGHT_GREEN,
  ORANGE,
  RED,
  WHITE,
} from '../../utils/Colors';
import {useNavigation} from '@react-navigation/native';
import {LoaderIndicator} from '../../common/LoaderIndicator';
import Network from '../../network/Network';
import {AuthContext} from '../../services/authProvider';
import {showToastMessage} from '../../utils';
import {DOCTOR_IMAGE_URL} from '../../utils/AppConstant';
import LinearGradient from 'react-native-linear-gradient';

const windowWidth = Dimensions.get('window').width;

const MyAppointments = props => {
  const navigation = useNavigation();
  const {user, token} = useContext(AuthContext);
  const [loading, setLoading] = useState(false);
  const [categoryData, setCategoryData] = useState([]);

  useEffect(() => {
    getMyAppointment();
  }, []);

  const getMyAppointment = date => {
    setLoading(true);
    Network('user/get-all-appointment', 'get', null, token)
      .then(async res => {
        console.log(' /n/n Result ', JSON.stringify(res.data));
        setLoading(false);
        setCategoryData(res.data.reverse());
      })
      .catch(error => {
        setLoading(false);
        showToastMessage(error);
      });
  };

  const endAppointment = appointmentId => {
    setLoading(true);
    let data = new FormData();
    data.append('appointment_id', appointmentId);
    Network('user/cancel-appointment', 'post', data, token)
      .then(async res => {
        console.log(' /n/n Result ', JSON.stringify(res));
        setLoading(false);
        showToastMessage(res.message);
        getMyAppointment();
      })
      .catch(error => {
        setLoading(false);
        showToastMessage(error);
      });
  };

  const renderItem = ({item}) => (
    <View
      style={{
        margin: 6,
        borderRadius: 10,
        flexDirection: 'column',
        backgroundColor: WHITE,
        justifyContent: 'center',
        padding: 10,
        marginTop: 10,
        marginHorizontal: 10,
        elevation: 3,
      }}>
      <View style={{flexDirection: 'row', marginTop: 10, alignItems: 'center'}}>
        {item.doctor_image !== null ? (
          <Image
            source={{uri: item.doctor_image}}
            style={{width: 60, height: 60, borderRadius: 30}}
          />
        ) : (
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={['#FAA41A', '#906445', '#28246F']}
            style={{
              padding: 10,
              width: 60,
              height: 60,
              borderRadius: 30,
            }}>
            <Image
              source={APP_ICON}
              style={{width: 40, height: 40, resizeMode: 'contain'}}
            />
          </LinearGradient>
        )}
        <View style={{flexDirection: 'column', marginStart: 20}}>
          <OVText size={medium} fontType={poppinsMedium} color={BLACK}>
            {item.doctor_name}
          </OVText>
          <OVText size={small} fontType={poppinsRegular} color={BLACK}>
            {item.appointment_type === 1 ? item.speciality : 'Vaccine'}
          </OVText>
          {item.language > 0 && (
            <OVText size={small} fontType={poppinsRegular} color={BLACK}>
              {item.language[0].name}
            </OVText>
          )}

          <OVText size={small} fontType={poppinsRegular} color={BLACK}>
            {item.exprience != null ? item.exprience : '0'} Years of Experiance
          </OVText>
        </View>
      </View>
      <View style={{flexDirection: 'row', marginTop: 20}}>
        <OVText
          size={medium}
          fontType={poppinsRegular}
          color={WHITE}
          style={{
            textAlign: 'center',
            backgroundColor: APP_THEME_COLOR,
            borderRadius: 6,
            paddingVertical: 10,
            marginEnd: 5,
            flex: 1,
            elevation: 3,
          }}>
          Date : {item.date}
        </OVText>

        <OVText
          size={medium}
          fontType={poppinsRegular}
          color={WHITE}
          style={{
            textAlign: 'center',
            backgroundColor: APP_THEME_COLOR,
            borderRadius: 6,
            paddingVertical: 10,
            marginStart: 10,
            flex: 1,
            elevation: 3,
          }}>
          Time : {item.time}
        </OVText>
      </View>
      <View
        style={{
          flexDirection: 'row',
          marginTop: 20,
        }}>
        <OVText
          onPress={() =>
            navigation.navigate('BookAppointments', {
              appointmentId: item.id,
              bookingType: item.appointment_type,
              id: item.user_id,
            })
          }
          size={small}
          fontType={poppinsRegular}
          color={WHITE}
          style={{
            backgroundColor: ORANGE,
            paddingHorizontal: 10,
            paddingVertical: 10,
            borderRadius: 20,
            elevation: 3,
            textAlign: 'center',
            flex: 1,
          }}>
          Rechedule
        </OVText>
        <OVText
          onPress={() => endAppointment(item.id)}
          size={small}
          fontType={poppinsRegular}
          color={WHITE}
          style={{
            backgroundColor: RED,
            paddingHorizontal: 10,
            paddingVertical: 10,
            borderRadius: 20,
            elevation: 3,
            flex: 1,
            marginHorizontal: 10,
            textAlign: 'center',
          }}>
          Cancel
        </OVText>
        <OVText
          onPress={() => navigation.navigate('Chat', {itemData: item})}
          size={small}
          fontType={poppinsRegular}
          color={WHITE}
          style={{
            backgroundColor: LIGHT_GREEN,
            paddingHorizontal: 20,
            paddingVertical: 10,
            borderRadius: 20,
            elevation: 3,
            textAlign: 'center',
            flex: 1,
          }}>
          Chat
        </OVText>
      </View>
    </View>
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: BG_COLOR}}>
      <ScrollView>
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
          }}>
          <FlatList
            data={categoryData}
            renderItem={renderItem}
            keyExtractor={item => item.id}
          />
        </View>
      </ScrollView>
      {loading && <LoaderIndicator loading={loading} />}
    </SafeAreaView>
  );
};

export default MyAppointments;
