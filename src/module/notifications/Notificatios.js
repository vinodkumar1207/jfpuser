/* eslint-disable react-native/no-inline-styles */
import {useNavigation} from '@react-navigation/native';
import React from 'react';
import {FlatList, SafeAreaView, View} from 'react-native';
import {Header} from '../../common/Header';
import OVText, {medium, poppinsRegular} from '../../components/OVText';
import {IMAGE_PET_SHOP} from '../../images';
import {BG_COLOR, GRAY_200, GRAY_700, WHITE} from '../../utils/Colors';

const categoryData = [
  {
    image: IMAGE_PET_SHOP,
    name:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    desc: 'General Vet \nEnglish \n21+ Yaers of Experiance',
  },
  {
    image: IMAGE_PET_SHOP,
    name:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    desc: 'General Vet \nEnglish \n21+ Yaers of Experiance',
  },
  {
    image: IMAGE_PET_SHOP,
    name:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    desc: 'General Vet \nEnglish \n21+ Yaers of Experiance',
  },
  {
    image: IMAGE_PET_SHOP,
    name:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    desc: 'General Vet \nEnglish \n21+ Yaers of Experiance',
  },
  {
    image: IMAGE_PET_SHOP,
    name:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    desc: 'General Vet \nEnglish \n21+ Yaers of Experiance',
  },
  {
    image: IMAGE_PET_SHOP,
    name:
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    desc: 'General Vet \nEnglish \n21+ Yaers of Experiance',
  },
];
const Notificatios = props => {
  const navigation = useNavigation();

  const renderItem = ({item, index}) => (
    <View
      style={{
        padding: 13,
        backgroundColor: index % 2 ? WHITE : GRAY_200,
      }}>
      <OVText size={medium} fontType={poppinsRegular} color={GRAY_700}>
        {item.name}
      </OVText>
    </View>
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: BG_COLOR}}>
      <Header
        isHome={false}
        navigation={navigation}
        onBackPressed={() => navigation.goBack()}
        title="Notifications"
      />
      <FlatList
        data={categoryData}
        renderItem={renderItem}
        keyExtractor={item => item.image}
      />
    </SafeAreaView>
  );
};

export default Notificatios;
