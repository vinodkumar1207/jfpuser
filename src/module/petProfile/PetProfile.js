/* eslint-disable react-native/no-inline-styles */
import {useNavigation} from '@react-navigation/native';
import React, {useState} from 'react';
import {
  Dimensions,
  Image,
  SafeAreaView,
  ScrollView,
  View,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {Header} from '../../common/Header';
import OVText, {
  extraSmall,
  poppinsMedium,
  poppinsRegular,
  small,
} from '../../components/OVText';
import {
  PET_AGE,
  PET_GENDER,
  PET_IMAGE_SMALL,
  PET_NAME,
  PET_TYPE,
  PET_WEIGHT,
  MEDICAL_RECORDS,
  VACCINATION,
  DEWORMING,
  PET_REPORTS,
  PET_SYRUP,
  PET_TABLET,
  FORWORD_ARROW,
} from '../../images';
import {
  APP_THEME_COLOR,
  BG_COLOR,
  BLACK,
  GRAY_300,
  GRAY_400,
  TEXT_COLOR_LIGHT,
  WHITE,
  YELLOW,
} from '../../utils/Colors';

const windowWidth = Dimensions.get('window').width;

const categoryData = [
  {
    image: PET_REPORTS,
    name: 'Chest X-Ray Report',
    date: '24/01/2021',
  },
  {
    image: PET_REPORTS,
    name: 'Chest X-Ray 1',
    date: '24/01/2021',
  },
  {
    image: PET_SYRUP,
    name: 'Syrup',
    date: '24/01/2021',
  },
  {
    image: PET_TABLET,
    name: 'Prescription',
    date: '24/01/2021',
  },
  {
    image: PET_REPORTS,
    name: 'Chest X-Ray Report',
    date: '24/01/2021',
  },
  {
    image: PET_REPORTS,
    name: 'Chest X-Ray 1',
    date: '24/01/2021',
  },
  {
    image: PET_SYRUP,
    name: 'Syrup',
    date: '24/01/2021',
  },
  {
    image: PET_TABLET,
    name: 'Prescription',
    date: '24/01/2021',
  },
];

const PetProfile = props => {
  const navigation = useNavigation();
  const [refresh, setRefresh] = useState(false);
  const [selectedItemType, setSelectedItemType] = useState(1);

  const renderItem = ({item, index}) => (
    <TouchableOpacity
      activeOpacity={1}
      onPress={() => navigation.navigate('LabResults')}>
      <View
        style={{
          flexDirection: 'column',
          padding: 10,
        }}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 10,
          }}>
          <Image source={item.image} />

          <OVText
            size={small}
            fontType={poppinsRegular}
            color={WHITE}
            style={{
              textAlign: 'left',
              marginStart: 10,
              flex: 1,
            }}>
            {item.name}
          </OVText>
          <OVText
            size={small}
            fontType={poppinsRegular}
            color={WHITE}
            style={{
              textAlign: 'right',
              marginStart: 10,
            }}>
            {item.date}
          </OVText>
          <Image
            source={FORWORD_ARROW}
            style={{tintColor: WHITE, marginStart: 10}}
          />
        </View>
        <View style={{height: 1, backgroundColor: WHITE, marginTop: 10}} />
      </View>
    </TouchableOpacity>
  );

  return (
    <SafeAreaView style={{flex: 1, backgroundColor: BG_COLOR}}>
      <Header
        isHome={false}
        navigation={navigation}
        onBackPressed={() => navigation.goBack()}
        title="Pet Profile"
      />
      <ScrollView>
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            backgroundColor: BG_COLOR,
          }}>
          <View
            style={{
              backgroundColor: WHITE,
              borderRadius: 10,
              elevation: 3,
              padding: 10,
              margin: 10,
              flexDirection: 'column',
            }}>
            <OVText
              size={extraSmall}
              fontType={poppinsMedium}
              color={TEXT_COLOR_LIGHT}
              style={{textAlign: 'right'}}>
              Last Visit: 24/01/2021
            </OVText>
            <Image source={PET_IMAGE_SMALL} style={{alignSelf: 'center'}} />
            <View
              style={{
                flexDirection: 'row',
                marginTop: 6,
                justifyContent: 'center',
              }}>
              <Image source={PET_NAME} />
              <OVText
                size={small}
                fontType={poppinsRegular}
                color={BLACK}
                style={{textAlign: 'right', marginStart: 10, marginEnd: 20}}>
                Noddy
              </OVText>
              <Image source={PET_TYPE} />
              <OVText
                size={small}
                fontType={poppinsRegular}
                color={BLACK}
                style={{textAlign: 'right', marginStart: 10}}>
                Labrador
              </OVText>
            </View>
            <View
              style={{height: 1, backgroundColor: GRAY_400, marginVertical: 10}}
            />
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                padding: 6,
              }}>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}>
                <Image source={PET_GENDER} />
                <OVText
                  size={small}
                  fontType={poppinsRegular}
                  color={BLACK}
                  style={{marginStart: 10}}>
                  Male
                </OVText>
              </View>
              <View
                style={{
                  width: 1,
                  backgroundColor: GRAY_400,
                  marginHorizontal: 10,
                }}
              />
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}>
                <Image source={PET_AGE} />
                <OVText
                  size={small}
                  fontType={poppinsRegular}
                  color={BLACK}
                  style={{marginStart: 10}}>
                  12 Months
                </OVText>
              </View>
              <View
                style={{
                  width: 1,
                  backgroundColor: GRAY_400,
                  marginHorizontal: 10,
                }}
              />
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}>
                <Image source={PET_WEIGHT} />
                <OVText
                  size={small}
                  fontType={poppinsRegular}
                  color={BLACK}
                  style={{marginStart: 10}}>
                  36kgs
                </OVText>
              </View>
            </View>
          </View>
          <View
            style={{
              backgroundColor: GRAY_300,
              padding: 10,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              activeOpacity={1}
              onPress={() => setSelectedItemType(1)}
              style={{
                flex: 1,
                borderRadius: 10,
                backgroundColor:
                  selectedItemType === 1 ? APP_THEME_COLOR : WHITE,
                padding: 10,
              }}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'column',
                  alignItems: 'center',
                }}>
                <Image
                  source={MEDICAL_RECORDS}
                  style={{
                    tintColor: selectedItemType === 1 ? WHITE : APP_THEME_COLOR,
                  }}
                />
                <OVText
                  size={small}
                  fontType={poppinsRegular}
                  color={selectedItemType === 1 ? WHITE : BLACK}
                  style={{marginTop: 10, flex: 1}}>
                  Medical
                </OVText>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={1}
              onPress={() => setSelectedItemType(2)}
              style={{
                flex: 1,
                borderRadius: 10,
                backgroundColor:
                  selectedItemType === 2 ? APP_THEME_COLOR : WHITE,
                padding: 10,
                marginHorizontal: 10,
              }}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'column',
                  alignItems: 'center',
                }}>
                <Image
                  source={VACCINATION}
                  style={{
                    tintColor: selectedItemType === 2 ? WHITE : APP_THEME_COLOR,
                  }}
                />
                <OVText
                  size={small}
                  fontType={poppinsRegular}
                  color={selectedItemType === 2 ? WHITE : BLACK}
                  style={{marginTop: 10}}>
                  Vaccination
                </OVText>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              activeOpacity={1}
              onPress={() => setSelectedItemType(3)}
              style={{
                flex: 1,
                borderRadius: 10,
                backgroundColor:
                  selectedItemType === 3 ? APP_THEME_COLOR : WHITE,
                padding: 10,
              }}>
              <View
                style={{
                  width: '100%',
                  flexDirection: 'column',
                  alignItems: 'center',
                }}>
                <Image
                  source={DEWORMING}
                  style={{
                    tintColor: selectedItemType === 3 ? WHITE : APP_THEME_COLOR,
                  }}
                />
                <OVText
                  size={small}
                  fontType={poppinsRegular}
                  color={selectedItemType === 3 ? WHITE : BLACK}
                  style={{marginTop: 10}}>
                  Deworming
                </OVText>
              </View>
            </TouchableOpacity>
          </View>
          <View
            style={{
              marginTop: 20,
              backgroundColor: YELLOW,
              borderTopEndRadius: 30,
              borderTopStartRadius: 30,
              padding: 10,
              flex: 1,
            }}>
            <FlatList
              data={categoryData}
              renderItem={renderItem}
              keyExtractor={item => item.image}
              style={{marginTop: 10}}
            />
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default PetProfile;
